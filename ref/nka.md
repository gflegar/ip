Nedeterminisicki konacni automat
================================

Definicija $NKA$
--------------
$NKA$ je uredjena petorka $NKA = (Q, \Sigma, \delta, q_0, F)$ gdje je:
* $Q$ -- skup stanja
* $\Sigma$ -- alfabet
* $\delta : Q \times \Sigma \rightarrow \mathcal{P}(Q)$ -- funkcija prijelaza
* $q_0 \in Q$ -- pocetno stanje
* $F \subseteq Q$ -- skup zavrsnih stanja

Konverzija $NKA$ u $DKA$
------------------------
Neka je zadan $NKA$ $M = (Q, \Sigma, \delta, q_0, F)$.
Zelimo konstruirati $DKA$ $M' = (Q', \Sigma', \delta', q_0', F')$ koji
prepoznaje isti jezik kao $M$.
To postizemo tako da stavimo:
* $Q' = 2^Q$
* $\Sigma' = \Sigma$
* $q_0' = \{ q_0 \}$
* $F' = \{ x \in 2^Q \vert F \cap x \neq \emptyset \}$
* $\delta'(x, s) = \bigcup\_{y \in x} \delta(y, s)$

Definicija $\epsilon-NKA$
-------------------------
$\epsilon-NKA$ je $NKA$ u cijem je alfabetu i prazan znak, tj. moguci su
prijelazi izmedju nekih stanja bez citanja znaka.

* $\epsilon-OKR(q) = \{ p \in Q \vert \text{do p se moze doci epsilon
  prijelazima iz q} \}$

* $\cap{\delta}(q, \epsilon) = \epsilon-OKR(q)$
* $\cap{\delta}(q, wa) = \epsilon-OKR(\bigcup\_{c \in \cap{\delta}(q, w)}
  \delta(c, a))$

Konstrukcija $NKA$ iz $\epsilon-NKA$
------------------------------------
* $Q' = Q$
* $\Sigma' = \Sigma$
* $q_0' = q_0$
* $F' = F' \cup \{q_0\} ako je u \epsilon okolini od q_0 stanje iz F$
* $\delta'(q, a) = \cap{\delta}(q, a)$


Regularni izrazi i pretvorba RI u konacni automat
=================================================

!!!!!!! ugasio se komp !!!!!!!!!!!!!!!!!!!!


### Svojstva regularnih jezika

Regularni jezici su zatvoreni na operacije
* unije             (r + s)
* nadovezivanja     (rs)
* Kleenov operator  (r\*)
* komplement
    M DKA za jezik L(M)
    Tada je automat za L(M)^c = \Sigma^* \setminus L(M):
    M' = (Q, \Sigma, \delta, q_0, Q \setminus F)
* presjek L \cap M = (L^c \cup M^c)^c
  ili (Q_1 \prod Q_2, \Sigma, ((q, p), a) -> ((\delta(q), \delta(p), a)), (q_1, q_2), F_1 \prod F_2)
* supstitucija (kada bismo u regularnom izrazu umjesto nekog znaka abecede ubacili regularan jezik
  opet bismo dobili regularan jezik

### Lema o pumpanju

Neka je L regularan jezik. Oznacimo s |v| duljinu rijeci v i s v\* = v...v (n puta).
kojeg prepoznaje neki DKA M s k stanja. Tada se bilo koja rijec z iz jezika s barem
k znakova moze razbiti na konkatenaciju oblika z = uvw gdje je
* |v| > 0
* |uv| \leq k
* uv^iw \in L za svaki i >= 0

### Dokaz
Za ulaznu rijec z \in L pratimo niz stanja kroz koja prolazi DKA.
U tom nizu ima barem k prijelaza pa postoji barem k+1 stanje.
Buduci da DKA ima k stanja slijedi da postoji stanje q koje se ponavlja.
Neka je q prvo takvo stanje. Neka je u dio rijeci prije prvog dolaska u stanje q.
Neka je v dio rijeci izmedju prvog i drugog dolaska u stanje q i neka je w
ostatak rijeci. Tada je ocito da je |v| > 0, |uv| \leq k i uv^iw.


### Primjer

Pokazimo da jezik 0^n1^n nije regularan.
Pretpostavimo da je jezik regularan. Tada mora vrijediti lema o pumpanju, tj.
postoji k takav da se svaka rijec s barem k znakova moze napisati kao uvw t.d. ...
Promotrimo rijec 0^k1^k. Ta se rijec moze razbiti na
* u = 0^i
* v = 0^j, j > 0
* w = 0^{n-i-j}1^n
Tada bi trebao biti i uv^iw iz jezika, ali je za npr. i = 2
uv^2w = 0^i0^{2j}0^{n-i-j}1^n = 0^{n+j}1^n sto je kontradikcija.

Neka je P skup palindroma nad abecedom [a-z]. Pokazimo da P nije regularan.
Pretpostavimo da je P regularan. Tada vrijedi lema o pumpanju, pa postoji k
td. ...
Ocito je a^kba^k iz jezika.
Isto kao i prije.... trebalo bi biti a^{k+i}ba^k iz jezika, sto je kontradikcija.

### Zadatak

L = {a^{i^2}} regularan?
L = {a^p | p prost}

