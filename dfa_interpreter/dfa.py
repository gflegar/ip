"""
This module defines a DFA class that provides operations for reading from file
and working with Deterministic Finite Automata.
"""

import xml.etree.ElementTree as ETree
from collections import deque

class DFA(object):
    """
    A class representing DFAs.
    """

    def __init__(self, filename):
        """
        Create a DFA from an xml file.
        """
        self._readfromXML(filename)

    def accepts(self, word):
        """
        Check if _word_ is accepted by this DFA.

        >>> dfa = DFA('examples/ones.xml')
        >>> dfa.accepts('11111')
        True
        >>> dfa.accepts('1101')
        False
        """
        state = self._starting
        for c in word:
            if (state, c) not in self._delta:
                return False
            state = self._delta[(state, c)]
        return state in self._accepting

    def accessible(self):
        """
        Get all accessible states.

        >>> dfa = DFA('examples/inaccessible.xml')
        >>> dfa.accessible()
        set(['a'])
        """
        accessible_ = set()
        queue = deque([self._starting])
        while queue:
            current = queue.popleft()
            if current in accessible_:
                continue
            accessible_.add(current)
            queue.extend(self._delta[(current, c)] for c in self._alphabet
                    if (current, c) in self._delta)
        return accessible_

    def inaccessible(self):
        """
        Get all inaccessible states.

        >>> dfa = DFA('examples/inaccessible.xml')
        >>> dfa.inaccessible()
        set(['b'])
        """
        return self._states - self.accessible()

    def equivalent(self):
        """
        Get matrix of equivalent states.
        """
        pass

    def minimize(self):
        """
        Minimize the DFA by removing inaccessible states.
        """
        inaccessible = self.inaccessible()
        self._states -= inaccessible
        self._accepting -= inaccessible
        for state, c in list(self._delta.keys()):
            if state in inaccessible:
                del self._delta[(state, c)]

    def write(self, filename):
        """
        Write the DFA to file named _filename_.
        """
        root = ETree.Element('dfa')

        ETree.SubElement(root, 'starting', {'name': self._starting})

        accepting = ETree.SubElement(root, 'accepting')
        for state in self._accepting:
            ETree.SubElement(accepting, 'state', {'name': state})

        delta = ETree.SubElement(root, 'delta')
        for key, value in self._delta.items():
            ETree.SubElement(delta, 'entry',
                    {'src': key[0], 'chr': key[1], 'dest': value})

        ETree.ElementTree(root).write(filename)

    def _readfromXML(self, filename):
        """
        Read a DFA from xml file.
        """
        root = ETree.parse(filename).getroot()
        self._starting = root.find('starting').get('name')
        self._accepting = { s.get('name') for s in root.find('accepting') }
        self._states = self._accepting | {self._starting}
        self._alphabet = set()
        self._delta = {}
        for entry in root.find('delta'):
            self._states |= {entry.get('src'), entry.get('dest')}
            for c in  entry.get('chr'):
                self._alphabet.add(c)
                self._delta[(entry.get('src'), c)] = entry.get('dest')

if __name__ == "__main__":
    import doctest
    doctest.testmod()

